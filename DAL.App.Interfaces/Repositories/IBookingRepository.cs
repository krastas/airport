﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IBookingRepository : IRepository<Booking>
    {
        Task<List<Booking>> FindBookingsById(int id);
        Task<Booking> FindBookingByIdWithIncludeAsync(int bookingId);
        Task<List<Booking>> AllBookingsWithAirportsAsync();

        Task<List<Booking>> AllValidBookingsWithAirportsAsync(int personId, bool isAdmin);
        Task<List<Booking>> AllNotValidBookingsWithAirportsAsync(int personId, bool isAdmin);
    }
}
