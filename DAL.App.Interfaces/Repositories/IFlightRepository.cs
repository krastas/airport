﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IFlightRepository : IRepository<Flight>
    {

        Task <List<Flight>> AllFlightsWithAirportsAsync();

        Task<List<Flight>> FindflightsWith2AirportsAsync(int airport1Id, int airport2Id);
        Task<List<Flight>> FindoldflightsAsync();
    }
    

}
