﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface IPersonRepository : IRepository<Person>
    {

        Task<Person> IfExsistsAsync(Person person);
    }
}
