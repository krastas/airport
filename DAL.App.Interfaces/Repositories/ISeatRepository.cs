﻿using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.Interfaces.Repositories
{
    public interface ISeatRepository : IRepository<Seat>
    {
        Task<List<Seat>> GetAllAvailableSeatsForPlaneAsync(int flightId, int planeId);
        Task<List<Seat>> GetListOffAllSeatsOfPlaneAsync();
        Task<List<Seat>> GetListOffAllSeatsOfPlaneAsync(int planeId);
    }
}
