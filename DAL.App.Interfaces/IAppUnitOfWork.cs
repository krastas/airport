﻿using DAL.App.Interfaces.Repositories;
using DAL.Interfaces;
using DAL.Interfaces.Repositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.App.Interfaces
{
    public interface IAppUnitOfWork : IUnitOfWork
    {
        IRepository<Airport> Airports { get; }
        IBookingRepository Bookings { get; }
        IFlightRepository Flights { get; }
        IFlightLineRepository FlightLines { get; }
        IPersonRepository People { get; }
        IPersonInSeatRepository PersonInSeats { get; }
        IPlaneRepository Planes { get; }
        IRepository<PlaneInAirport> PlaneInAirports { get; }
        ISeatRepository Seats { get; }


}
}
