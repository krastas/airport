﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Dto;
using BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using DAL.App.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Airport_angular.Controllers.api
{
    /// <summary>
    /// Controller for managing planes
    /// </summary>
    [Produces("application/json")]
    [Route("api/Planes")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PlanesController : Controller
    {

        private readonly IAppUnitOfWork _uow;
        private readonly IBookingService _bookingService;


        public PlanesController(IAppUnitOfWork uow, IBookingService bookingService)
        {
            _uow = uow;
            _bookingService = bookingService;
        }

        /// <summary>
        /// getting all planes
        /// </summary>
        /// <returns>list of planes</returns>
        /// <response code="401">user is not admin or logged in</response>
        /// <response code="404">Error finding planes</response>
        /// <response code="200">Found planes, returning</response>
        [Route("All")]
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetPlanes()
        {
            var planes =  await _uow.Planes.AllAsync();
            if (planes == null)
            {
                return NotFound();
            }

            return Ok(planes);
        }
        /// <summary>
        /// adds plane
        /// </summary>
        /// <param name="plane">plane id from query param</param>
        /// <returns>added plane</returns>
        /// <response code="400">plane object was ininvalidvlaid</response>
        /// <response code="401">user is not admin or logged in</response>
        /// <response code="200">adding was succsesful</response>
        /// <response code="500">Error adding plane</response>
        [Route("Add")]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddPlane([FromBody] Domain.Plane plane)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _uow.Planes.Add(plane);
            for (var i = 1; i < plane.PlaneSeatsNumber + 1; i++)
            {
                _uow.Seats.Add(new Seat()
                {
                    PlaneId = plane.PlaneId,
                    SeatNumber = i
                });
            }
            await _uow.SaveChangesAsync();
            return Ok(plane);

        }
        /// <summary>
        /// deletes planes
        /// </summary>
        /// <param name="id">plane id, which is query parameter </param>
        /// <returns>statusocde</returns>
        /// <response code="401">user is not admin or logged in</response>
        /// <response code="400">error occured while tyring to validate query param</response>
        /// <repsonse code="404">Error occured while trying to find plane or seats</repsonse>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeletePlane([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var plane = await _uow.Planes.FindAsync(id);
            if (plane == null)
            {
                return NotFound("Could not found plane by id");
            }
            var seats = await _uow.Seats.GetListOffAllSeatsOfPlaneAsync(plane.PlaneId);
            if (seats == null)
            {
                return NotFound("could not found seats by plane id");
            }
            _uow.Planes.Remove(plane);
            seats.ForEach(s => _uow.Seats.Remove(s));

            await _uow.SaveChangesAsync();
            return Ok();
        }
        /// <summary>
        /// get method for getting available seats for flight
        /// </summary>
        /// <param name="flightId">flight id of chosen flight</param>
        /// <param name="planeId">plane id, which are used in flight</param>
        /// <returns>successcode if everything is good, otherwise 404</returns>
        [HttpGet("findSeats/{flightId}/{planeId}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetSeats(int flightId, int planeId)
        {
            var availableSeats = await _bookingService.GetListOfSeatsForBookingAsync(flightId, planeId);
            if (availableSeats.Count == 0)
            {
                return BadRequest();
            }
            return Ok(availableSeats);
        }

        /// <summary>
        /// Finds seats for flight, which havent been booked yet
        /// </summary>
        /// <returns></returns>
        [HttpGet("findSeat")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllSeats()
        {
            var availableSeats = await _bookingService.GetListOfAllSeatsForBookingAsync();
            if (availableSeats.Count == 0)
            {
                return BadRequest();
            }
            return Ok(availableSeats);
        }
    }
}