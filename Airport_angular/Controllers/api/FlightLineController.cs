﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Dto;
using BL.Services;
using DAL.App.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Airport_angular.Controllers.api
{
    /// <summary>
    /// Controller for managing flight lines
    /// </summary>
    [Produces("application/json")]
    [Route("api/FlightLine")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    public class FlightLineController : Controller
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IFlightLineService _flightLineService;

        public FlightLineController(IAppUnitOfWork uow, IFlightLineService flightLineService)
        {
            _uow = uow;
            _flightLineService = flightLineService;
        }
        /// <summary>
        /// finds list of flight lines with included airports
        /// </summary>
        /// <returns>list of flight lines</returns>
        /// <response code="401">user is not admin or logged in</response>
        /// <response code="200">returns flight lines</response>
        /// <response code="404">find flightline gave error</response>
        [Route("All")]
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetFlightLines()
        {
            var flightLines =  await _flightLineService.GetAllFlightLinesWithAirportsAsync();
            if (flightLines == null)
            {
                return NotFound();
            }

            return Ok(flightLines);
        }

        /// <summary>
        /// Add flighline
        /// </summary>
        /// <param name="flightLine">flighline object</param>
        /// <returns>flightline</returns>
        /// <response code="401">user is not admin or logged in</response>
        /// <response code="200">adding was succsesful returning flightline</response>
        /// <response code="400">fligtline object has some missing properties or wrong data type</response>
        /// <response code="500">Error occurde while adding flightline</response>
        [Route("Add")]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddFlightLine([FromBody] Domain.FlightLine flightLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _uow.FlightLines.Add(flightLine);
            await _uow.SaveChangesAsync();
            return Ok(flightLine);
        }

        /// <summary>
        /// Deleting flightline
        /// </summary>
        /// <param name="id">flightline id with query param</param>
        /// <returns>flightline</returns>
        /// <response code="401">user is not admin or logged in</response>
        /// <response code="200">Delete succsesful</response>
        /// <response code="400">id was invalid</response>
        /// <response code="500">Error occured while deleting flightline</response>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteFlightLine([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var flightLine = await _uow.FlightLines.FindAsync(id);
            if (flightLine == null)
            {
                return NotFound();
            }

            _uow.FlightLines.Remove(flightLine);
            await _uow.SaveChangesAsync();
            return Ok();
        }
      }
}