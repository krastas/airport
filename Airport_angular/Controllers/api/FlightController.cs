﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.Dto;
using BL.Services;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Airport_angular.Controllers.api
{
    /// <summary>
    /// Controller for finding adding, deleting flights
    /// </summary>
    [Produces("application/json")]
    [Route("api/Flight")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class FlightController : Controller
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IFlightsService _flightsService;

        public FlightController(IAppUnitOfWork uow, IFlightsService flightsService)
        {
            _uow = uow;
            _flightsService = flightsService;
        }

        /// <summary>
        /// Find flights with included airports
        /// </summary>
        /// <returns>airport</returns>
        /// <repsonse code="200">Flight found, returning</repsonse>
        /// <repsonse code="404"> Could not find any flights</repsonse>
        /// <response code="401">user is not logged in or admin</response>
        [Route("All")]
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetFlights()
        {
            var flights =  await _flightsService.GetAllFlightsWithAirportsAsync();
            if (flights == null)
            {
                return NotFound("Not found");
            }

            return Ok(flights);
        }
        /// <summary>
        /// Adds flights
        /// </summary>
        /// <param name="flight">Flight object</param>
        /// <returns>flight with statuscode</returns>
        /// <repsonse code="400">Flight object has some fields missing or wrong data type</repsonse>
        /// <repsonse code="200"> Adding was succsesful, returning flight</repsonse>
        /// <repsonse code="500">Adding flight made unexcpected error</repsonse>
        /// <response code="401">user is not admin or logged in</response>
        [Route("Add")]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddFlight([FromBody] Domain.Flight flight)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _uow.Flights.Add(flight);
            await _uow.SaveChangesAsync();
            return Ok(flight);

        }
        /// <summary>
        /// Deletes Flights
        /// </summary>
        /// <param name="id">flight id form query parameter</param>
        /// <returns>statuscode</returns>
        /// <repsonse code="200">flight was succsesfully deleted </repsonse>
        /// <repsonse code="400">id is wrong data type</repsonse>
        /// <repsonse code="404">Flight not found</repsonse>
        /// <response code="401">user is not admin or logged in</response>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteFlight([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var flight = await _uow.Flights.FindAsync(id);
            if (flight == null)
            {
                return NotFound();
            }

            _uow.Flights.Remove(flight);
            await _uow.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Get method to get Flight DateTimes for booking form
        /// </summary>
        /// <param name="airport1Id">starting point</param>
        /// <param name="airport2Id">ending point</param>
        /// <returns>successcode if everything is good, otherwise 404</returns>
        /// <response code="404">'somethign went wrong, couldnt find any flghts</response>
        /// <response code="200">Returns list of flights</response>
        [HttpGet("find/{airport1Id}/{airport2Id}")]
        [AllowAnonymous]
        public async Task<IActionResult> FindFlightsWtih2Airports(int airport1Id, int airport2Id)
        {
            var flights =  await _flightsService.GetAllFlightsWithTwoAirportsAsync(airport1Id, airport2Id);
            if (flights == null)
            {
                return NotFound();
            }

            return Ok(flights);
        }
        /// <summary>
        /// Finds flights for statistics
        /// </summary>
        /// <returns>returns list of flights</returns>
        /// <repsonse code="200">flights were found</repsonse>
        /// <repsonse code="400">Couldnt find any flights</repsonse>
        /// <response code="401">user is not admin or logged in</response>
        [HttpGet("findold")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> FindoldFlights()
        {
            var bookingFlightDetailsDto =  await _flightsService.GetAllOldFlightsAsync();
            if (bookingFlightDetailsDto == null)
            {
                return BadRequest("Validation was missing");
            }

            return Ok(bookingFlightDetailsDto);
        }
    }
}