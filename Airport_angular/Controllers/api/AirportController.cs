﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.App.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Domain;
using BL.Dto;
using BL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Airport_angular.Controllers.api
{   /// <summary>
    /// Controller for finding airports
    /// </summary>
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("api/Airport")]
    public class AirportController : Controller
    {
        private readonly IAppUnitOfWork _uow;
        private readonly ILocationService _service;
        public AirportController(IAppUnitOfWork uow, ILocationService service)
        {
            _uow = uow;
            _service = service;
        }
        /// <summary>
        /// method to find airports for selectlist
        /// </summary>
        /// <returns>airportsDto</returns>
        /// <response code="200">returning list of airports</response>
        /// <response code="404">Could not find any airports</response>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAirports()
        {
            var airports = await  _service.GetAllAirportLocations();
            if (airports == null)
            {
                return NotFound("Could not find any airports");
            }

            return Ok(airports);
        }

        /// <summary>
        /// Gets all airports for admin view
        /// </summary>
        /// <returns>returns list of airports</returns>
        /// <response code="200">Everything went well, returning list of airports</response>
        /// <repsonse code="404">No airports found</repsonse>
        /// <response code="401">user is not admin or logged in</response>
        [Route("All")]
        [HttpGet]
        [Authorize(Roles = "Admin")]

        public async Task<IActionResult> GetAllAirports()
        {
            var airports =  await _uow.Airports.AllAsync();
            if (airports == null)
            {
                return NotFound();
            }

            return Ok(airports);
        }

        /// <summary>
        /// Adds airport
        /// </summary>
        /// <param name="airport">new Airport</param>
        /// <returns>returns added airport</returns>
        /// <response code="400">Data was not correct, some fields are missing or not right</response>
        /// <response code="200">Airport was saved to databse, return saved airport</response>
        /// <response code="500">Couild not save airport, something went wrong</response>
        /// <response code="401">user is not admin or logged in</response>
        [Route("Add")]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddAirport([FromBody] Domain.Airport airport)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _uow.Airports.Add(airport);
            await _uow.SaveChangesAsync();
            return Ok(airport);

        }
        /// <summary>
        /// Deleting airports
        /// </summary>
        /// <param name="id">Airport id in query parmaeter</param>
        /// <returns>statuscode</returns>
        /// <response code="200">Airport was deleted succsesfully</response>
        /// <response code="500">Something went wrong with deleting</response>
        /// <response code="404">model was not validated, input have missing fields or worng data type</response>
        /// <response code="401">user is not admin or logged in</response>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteAirport([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var airport = await _uow.Airports.FindAsync(id);
            if (airport == null)
            {
                return NotFound();
            }

            _uow.Airports.Remove(airport);
            await _uow.SaveChangesAsync();
            return Ok();
        }
        /// <summary>
        /// fidning a airport
        /// </summary>
        /// <param name="id">airport id from query parameter</param>
        /// <returns>airport</returns>
        ///  <response code="200">Airport was found, returning airport</response>
        /// <response code="404">Airport was not found with that id </response>
        /// <response code="401">user is not logged in</response>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> FindAirportById(int id)
        {
            var airport = await _uow.Airports.FindAsync(id);
            if (airport == null)
            {
                return NotFound("Could not find any airport with that id");
            }

            return Ok(airport);
        }
    }
}