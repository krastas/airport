﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BL.Dto;
using BL.Services;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Airport_angular.Controllers.api
{
    /// <summary>
    /// controller for handling bookings
    /// </summary>
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("api/Bookings")]
    public class BookingsController : Controller
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IBookingService _bookingService;
        private readonly Microsoft.AspNetCore.Identity.UserManager<UserAccount> _userManager;

        public BookingsController(IAppUnitOfWork uow, IBookingService bookingService,
            Microsoft.AspNetCore.Identity.UserManager<UserAccount> userManager)
        {
            _uow = uow;
            _bookingService = bookingService;
            _userManager = userManager;
        }

        /// <summary>
        /// Finds all bookings with included airports
        /// </summary>
        /// <returns>list of airports</returns>
        /// <response code="200">returns list of airports</response>
        /// <response code="404">Could not find bookings, something went wrong</response>
        /// <response code="401">user is not admin or logged in</response>
        [Route("All")]
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetBookings()
        {
            var bookings = await _bookingService.GetAllBookingsWithAirportsAsync();
            if (bookings == null)
            {
                return NotFound("Could not found any, something went wrong");
            }

            return Ok(bookings);
        }

        /// <summary>
        /// Find bookings by airport
        /// </summary>
        /// <param name="id"> query parameter airport id</param>
        /// <returns>returns list of bookings</returns>
        /// <response code="200">returning list of bookings by 1 airport id</response>
        /// <response code="404">Could not find any bookings, somethiong went wrong</response>
        /// <response code="400">given query id was bad, wrong datatype or wrong number</response>
        /// <response code="401">user is not admin or logged in</response>
        [HttpGet("{Id:int}")]
        [Authorize(Roles = "Admin, User")]
        public async Task<IActionResult> GetBookings(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Id was in wrong datatype");
            }

            var bookings = await _uow.Bookings.FindBookingsById(id);
            if (bookings == null)
            {
                return NotFound("Something went wrong");
            }

            return Ok(bookings);
        }

        /// <summary>
        /// Post method to book a ticket for a plane
        /// </summary>
        /// <param name="booking"> booking object with person, who booked it</param>
        /// <returns>added booking object</returns>
        /// <response code="400">Could not add, data was no validated</response>
        /// <response code="200">Post was sumitted and return added booking object</response>
        /// <response code="500">Could not post booking, something went wrong</response>
        [HttpPost("book")]
        [AllowAnonymous]
        public async Task<IActionResult> BookBooking([FromBody] Booking booking)
        {
            if (booking == null)
            {
                return BadRequest();
            }


            if (!ModelState.IsValid)
            {
                // ask if user is logged in
                if (HttpContext.User.Identity.IsAuthenticated)
                {
                    var user = await _userManager.FindByEmailAsync(HttpContext.User.Identity.Name);
                    var person = _uow.People.Find(user.PersonId);
                    booking.Person = person;
                }

                // try to validate model again
                ModelState.Clear();
                TryValidateModel(booking);
                // ask if validation is invalid
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }
            }

            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                var ifPErsonExsists = await _uow.People.IfExsistsAsync(booking.Person);
                if (ifPErsonExsists == null)
                {
                    _uow.People.Add(booking.Person);
                }
            }

            _uow.Bookings.Add(booking);
            _uow.PersonInSeats.Add(new PersonInSeat()
            {
                FlightId = booking.FlightId,
                Person = booking.Person,
                SeatId = booking.SeatId
            });
            await _uow.SaveChangesAsync();
            return Ok(_bookingService.GetAddedBookingAsBookingDtoAsync(booking.BookingId));
        }

        /// <summary>
        /// deletes a airport
        /// </summary>
        /// <param name="id">airport id form query parameter</param>
        /// <returns>statuscode</returns>
        /// <response code="401">user is not logged in or admin</response>
        /// <response code="400">Given id invalid</response>
        /// <response code="404">Could not find any bookings, something went wrong</response>
        /// <response code="200">returnin succsesful statuscode</response>
        [Authorize(Roles = "Admin, User")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBooking([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var booking = await _uow.Bookings.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }
            var pis = await _uow.PersonInSeats.FindPersonInSeatBySeatIdAsync(booking.SeatId);
            
            _uow.Bookings.Remove(booking);
            _uow.PersonInSeats.Remove(pis);
            await _uow.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// finds list on bookings, which are active
        /// </summary>
        /// <returns>returns active booking list</returns>
        /// <response code="200">List of bookings which are acitve</response>
        /// <response code="400">could not find any active bookings, something went wrong</response>
        /// <response code="404">User was invalid</response>
        ///  <response code="401">user is not logged in</response>
        [HttpGet("findvalid")]
        public async Task<IActionResult> FindValidBookings()
        {
            List<BookingDto> bookings;
            var user = await _userManager.FindByEmailAsync(HttpContext.User.Identity.Name);
            if (user == null)
            {
                return BadRequest("Could not find user");
            }

            var isAdmin = HttpContext.User.IsInRole("Admin");
            if (isAdmin)
            {
                bookings = await _bookingService.GetAllValidBookingsWithAirportsAsync(user.PersonId, false);
            }
            else
            {
                bookings = await _bookingService.GetAllValidBookingsWithAirportsAsync(user.PersonId, false);
                if (bookings == null)
                {
                    return NotFound("Could not find any bookings");
                }
            }

            return Ok(bookings);
        }

        /// <summary>
        /// Method to fiind user bookings what are old
        /// </summary>
        /// <returns>list of bookings</returns>
        /// <response code="200">List of bookings which are older than today</response>
        /// <response code="400">could not find any bookings with older date than today</response>
        /// <response code="404">User was invalid</response>
        ///  <response code="401">user is not logged in</response>
        [HttpGet("findnotvalid")]
        public async Task<IActionResult> FindNotValidBookings()
        {
            List<BookingDto> bookings;
            var user = await _userManager.FindByEmailAsync(HttpContext.User.Identity.Name);
            if (user == null)
            {
                return BadRequest("Could not found user");
            }

            var isAdmin = HttpContext.User.IsInRole("Admin");
            if (isAdmin)
            {
                bookings = await _bookingService.GetAllNotValidBookingsWithAirportsAsync(user.PersonId, true);
                if (bookings == null)
                {
                    return NotFound("Could not find bookings");
                }
            }
            else
            {
                bookings = await _bookingService.GetAllNotValidBookingsWithAirportsAsync(user.PersonId, false);
                if (bookings == null)
                {
                    return NotFound("Could not find bookings");
                }
            }

            return Ok(bookings);
        }
    }
}