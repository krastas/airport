﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Airport_angular.Models.AccountViewModels;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Airport_angular.Controllers.api
{
    /// <summary>
    /// Controller for user managment
    /// </summary>
    [Produces("application/json")]
    [Route("api/Security")]
    [Authorize(Roles = "Admin")]
    public class SecurityController : Controller
    {
        private readonly SignInManager<UserAccount> _signInManager;
        private readonly UserManager<UserAccount> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IAppUnitOfWork _uow;
        private readonly RoleManager<IdentityRole> _roleManager;

        public SecurityController(SignInManager<UserAccount> signInManager,
            UserManager<UserAccount> userManager, IConfiguration configuration, IAppUnitOfWork uow, RoleManager<IdentityRole> roleManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _configuration = configuration;
            _uow = uow;
            _roleManager = roleManager;
        }
        /// <summary>
        /// Register user and return token
        /// </summary>
        /// <param name="vm">Register data</param>
        /// <returns>token or error</returns>
        /// <response code="200">Everyhting went well, token generated</response>
        /// <response code="400">For some reason could not register user, this persoin has accont already</response>
        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterUser([FromBody] RegisterViewModel vm)
        {
            if (ModelState.IsValid)
            {
               
                
                var person = new Person()
                {
                    FirstName = vm.FirstName,
                    LastName = vm.LastName,
                    PersonIdCode = vm.IdCode,
                };

                var ifPersonExsists = await _uow.People.IfExsistsAsync(person);
                if (ifPersonExsists != null)
                {
                    return BadRequest("This person has already accont");
                }
                var user = new UserAccount() { UserName = vm.Email, Email = vm.Email, Person = person,
                    FirstName = vm.FirstName, LastName = vm.LastName, Since = DateTime.Now};
                var result = await _userManager.CreateAsync(user, vm.Password);
                if (result.Succeeded)
                {
                    var role = await _roleManager.FindByNameAsync("User");
                    if (role != null)
                    {
                        await _userManager.AddToRoleAsync(user, "User");
                        await _userManager.RemoveFromRoleAsync(user, "Admin");
                    }
                    return await GetToken( new LoginViewModel()
                 {
                     Password = vm.Password,
                     Email = vm.Email
                 });
                }             
            }
            return BadRequest("Could not register user");

        }
        /// <summary>
        /// makes user to admin
        /// </summary>
        /// <returns>statuscode</returns>
        ///  <response code="200">role succsesfully changed</response>
        /// <response code="400">Could not change accont role, no user found</response>
        /// <response code="401">user is not admin or logged in</response>
        [HttpPost("{email}")]
        [Route("makeAdmin")]
        public async Task<IActionResult> MakeToAdmin(string email)
        {
            //get user
          var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return BadRequest("No user found");
            }


            await _userManager.AddToRoleAsync(user, "Admin");
            return Ok();
        }
        /// <summary>
        /// makes accont to user role
        /// </summary>
        /// <returns>succsescode</returns>
        /// <response code="200">role succsesfully changed</response>
        /// <response code="400">Could not change accont role, no user found</response>
        /// <response code="401">user is not admin or logged in</response>
        [HttpPost("{email}")]
        [Route("makeToUser")]
        public async Task<IActionResult> MakeToUser([FromRoute]string email)
        {
            //get user
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return NotFound("User not found");
            }

            await _userManager.AddToRoleAsync(user, "User");
            return Ok();
        }


        /// <summary>
        /// Loging in and generating token for user
        /// </summary>
        /// <param name="loginViewModel">login data</param>
        /// <returns>jwt token</returns>
        /// <repsonse code="200">Token returned</repsonse>
        /// <repsonse code="400">Could not sign in</repsonse>
        [HttpPost]
        [Route("getToken")]
        [AllowAnonymous]
        public async Task<IActionResult> GetToken([FromBody] LoginViewModel loginViewModel)
        {
            // check for user existance
            var user = await _userManager.FindByEmailAsync(loginViewModel.Email);
            if (user != null)
            {
                // check for password validity
                var result = await _signInManager
                    .CheckPasswordSignInAsync(user, loginViewModel.Password, false);
                if (result.Succeeded)
                {
                    var options = new IdentityOptions();

                    // add standard claims
                    var claims = new List<Claim>()
                    {
                        // subject
                        new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                        // unique id for this token
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        // as.net specific claims
                        // user id
                        new Claim(options.ClaimsIdentity.UserIdClaimType, user.Id),
                        // username
                        new Claim(options.ClaimsIdentity.UserNameClaimType, user.UserName),
                        // person name 
                        new Claim("Name", user.FirstLast)

                    };

                    // get all the misc claims for user and add them also
                    var userClaims = await _userManager.GetClaimsAsync(user);
                    claims.AddRange(userClaims);

                    // get and add the role and role claims
                    var userRoles = await _userManager.GetRolesAsync(user);
                    foreach (var userRole in userRoles)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, userRole));
                        var role = await _userManager.FindByNameAsync(userRole);
                        if (role != null)
                        {
                            var roleClaims = await _userManager.GetClaimsAsync(role);
                            foreach (Claim roleClaim in roleClaims)
                            {
                                claims.Add(roleClaim);
                            }
                        }
                    }

                    // generate signing key
                    var key = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(_configuration["Token:Key"]));
                    // generate signing credentials
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    var token = new JwtSecurityToken(
                        // issuer
                        // issuer
                        _configuration["Token:Issuer"],
                        // audience
                        _configuration["Token:Issuer"],
                        // all the claims
                        claims,
                        // lifetime of the token
                        expires: DateTime.Now.AddYears(2),
                        // signature creation info
                        signingCredentials: creds
                        );

                    // resulting anonymous object, with base64 encoded jwt token
                    var res = new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(token)
                    };

                    // serialise result and return it
                    return Ok(res);
                }
            }
            return BadRequest("Could not create token");
        }

        
    }
}
