﻿using System.Threading.Tasks;

namespace Airport_angular.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
