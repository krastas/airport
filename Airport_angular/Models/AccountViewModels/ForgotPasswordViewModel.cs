﻿using System.ComponentModel.DataAnnotations;

namespace Airport_angular.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
