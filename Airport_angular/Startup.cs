using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airport_angular.Services;
using BL.Factories;
using BL.Services;
using DAL.App.EF;
using DAL.App.EF.Helpers;
using DAL.App.Interfaces;
using DAL.App.Interfaces.Helpers;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;

namespace Airport_angular
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<DAL.App.EF.ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<UserAccount
                    , IdentityRole>()
                .AddEntityFrameworkStores<DAL.App.EF.ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddScoped<IDataContext, ApplicationDbContext>();
            services.AddScoped<IRepositoryProvider, EFRepositoryProvider>();
            services.AddSingleton<IRepositoryFactory, EFRepositoryFactory>();
            services.AddScoped<IAppUnitOfWork, AppEFUnitOfWork>();
            services.AddTransient<ILocationFactory, LocationFactory>();
            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<IFlightsFactory, FlightsFactory>();
            services.AddTransient<IFlightsService, FlightsService>();
            services.AddTransient<IFlightLineFactory, FlightLineFactory>();
            services.AddTransient<IFlightLineService, FlightLineService>();
            services.AddTransient<IBookingFactory, BookingFactory>();
            services.AddTransient<IBookingService, BookingService>();
            services.AddMvc();

            #region jsonconfiguration
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling
                    = Newtonsoft.Json.ReferenceLoopHandling.Serialize;
                options.SerializerSettings.PreserveReferencesHandling
                    = Newtonsoft.Json.PreserveReferencesHandling.Objects;
                options.SerializerSettings.Formatting
                    = Newtonsoft.Json.Formatting.Indented;
            });
            #endregion



            #region JWT Bearer

            

            services.AddAuthentication()
                  .AddCookie(options => { options.SlidingExpiration = true; })
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.RequireHttpsMetadata = false;

                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidIssuer = Configuration["Token:Issuer"],
                        ValidAudience = Configuration["Token:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(Configuration["Token:Key"])
                        )
                    };

                    // if you fish to modify identity (ie validate, that use is not banned)
                    options.Events = new JwtBearerEvents
                    {
                        OnTokenValidated = async (context) =>
                        {


                            // lets check from usermanager, is the user actually allowed into system
                            var userManager = context.HttpContext.RequestServices.GetService<UserManager<UserAccount>>();
                            var user = await userManager.FindByEmailAsync(context.Principal.Identity.Name);
                            if (user == null || user.LockoutEnd > DateTime.Now)
                            {
                                context.Response.StatusCode = 401;
                            }


                        }
                    };
                });

            #endregion
            #region swagger

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "MagicAirlane airport", Version = "v1", Description = "This is magic airport web api documentation"});
            });

            #endregion

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Airport");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
