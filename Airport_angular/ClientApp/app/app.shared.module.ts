import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';




import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { BookingComponent } from './components/Booking/booking.component';
import { ManageComponent } from './components/Manage/manage.component';
import { TicketsComponent } from './components/Tickets/tickets.component';
import { LoginComponent } from './components/Login/login.component';
import { RegisterComponent } from './components/Register/register.component';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { ErrorComponent } from './components/Error/Errorpage.component';
import { AuthGuardService } from './auth/auth.guard';
import { AuthService } from './auth/auth.service';
import { AdminGuard } from './auth/admin.guard';




@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        BookingComponent,
        ManageComponent,
        TicketsComponent,
        LoginComponent,
        RegisterComponent,
        StatisticsComponent,
        ErrorComponent
    ],
    providers: [AuthGuardService, AuthService, AdminGuard],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
     
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'booking', component: BookingComponent },
            { path: 'manage', component: ManageComponent, canActivate: [AdminGuard] },
            { path: 'tickets', component: TicketsComponent, canActivate: [AuthGuardService] },
            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent },
            { path: 'statistics', component: StatisticsComponent },
            { path: 'error', component: ErrorComponent },
            { path: '**', redirectTo: 'error' }
        ])
    ]
})
export class AppModuleShared {
}
