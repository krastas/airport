import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppModuleShared } from './app.shared.module';
import { AppComponent } from './components/app/app.component';
import { FormsModule } from '@angular/forms';
import { AuthService } from './auth/auth.service';





@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        BrowserModule,
        AppModuleShared,
        FormsModule,
        

    ],
    providers: [
        AuthService,
        {provide: 'BASE_URL', useFactory: getBaseUrl}
    ]
})
export class AppModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
