﻿import { Injectable, Inject, PLATFORM_ID} from '@angular/core';
import { User } from './model/user';
import { Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { RegisterUser } from './model/registerUser';
import { isPlatformBrowser } from '@angular/common';
import { Router } from "@angular/router";


@Injectable() 
export class AuthService {
    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, @Inject(PLATFORM_ID) private platformId: Object, public router: Router) { }

    login(user: User): Promise<boolean> {
        console.log(user);
        return this.http.post(this.baseUrl + '/api/Security/getToken', user).toPromise<any>()
            .then((res) => {
                localStorage.setItem('token', res.json().token);
                this.router.navigateByUrl('/home');
                return true;
            });
    }

    register(user: RegisterUser): Promise<void> {
        return this.http.post(this.baseUrl + 'api/Security/register', user).toPromise()
            .then((res) => {
                localStorage.setItem('token', res.json().token);
                this.router.navigateByUrl('/home');
            });
    }

    logout() {     
            localStorage.removeItem('token');
    }

    isAuthincated() {
        if (isPlatformBrowser(this.platformId)) {
            if (localStorage.getItem('token') == null || localStorage.getItem('token') == undefined ) {
                return false;
            }
            let jwt = this.getParsedToken();
            let currentTime = new Date().getTime() / 1000;
            if (currentTime > jwt.exp) {
                this.logout();
                return false;
            } else {
                return true;
            }

        }
        return false;
    }

    getToken(): string {
        if (this.isAuthincated()) {
        var token = localStorage.getItem('token');
        if (token) {
            return token;
        } else {
            return ' ';
            }
        }
        return '';
    }

    getParsedToken() {
        var token = localStorage.getItem('token');
        let jwtData = (token as string).split('.')[1];
        let decodedJwtJsonData = window.atob(jwtData);
        return  JSON.parse(decodedJwtJsonData);
    }

    getName() {

        let decodedJwtData = this.getParsedToken();
        return decodedJwtData.Name;
    }

    isAdmin() {
        if (this.isAuthincated()) {
            let jwtData = (this.getToken() as string).split('.')[1];
            let decodedJwtJsonData = window.atob(jwtData);
            let decodedJwtData = JSON.parse(decodedJwtJsonData);
            let isInRole = decodedJwtData['http://schemas.microsoft.com/ws/2008/06/identity/claims/role']; 
            if (isInRole === undefined) {
                return false;
            }
            if (typeof isInRole === 'string') {
                return isInRole === "Admin";
            }
            return isInRole.find((x: any) => x === "Admin");
        } else {
            return false;
        }
    }
}
