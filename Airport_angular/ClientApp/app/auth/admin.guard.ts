﻿import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable()
export class AdminGuard implements CanActivate {
    constructor(public auth: AuthService, public router: Router) { }
    canActivate(): boolean {
        if (!this.auth.isAuthincated()) {
            this.router.navigate(['login']);
            return false;
        }
        if (!this.auth.isAdmin()) {
            this.router.navigate(['login']);
            return false;
        }
        return true;
    }
}