﻿export class RegisterUser {
    email: string;
    password: string;
    confirmPassword: string;
    firstName: string;
    lastName: string;
    idCode: string;

    constructor(email: string, password: string, confirmPassword: string, firstName: string, lastName: string, idCode: string) {
        this.email = email;
        this.idCode = idCode;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}