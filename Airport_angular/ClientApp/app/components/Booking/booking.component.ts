﻿import { Component, Inject, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as Booking from "./Models/booking";
import IBookingFrom = Booking.IBookingFrom;
import IFlightDropDown = Booking.IFlightDropDown;
import ISeats = Booking.ISeats;
import IAirport = Booking.IAirport;
import IFlight = Booking.IFlight;
import IPlane = Booking.IPlane;
import { AuthService } from '../../auth/auth.service';
import * as Booking1 from "../Tickets/Model/model";
import IBooking = Booking1.IBooking;


@Component({
    selector: 'booking',
    templateUrl: './booking.component.html'
})
export class BookingComponent implements OnInit {
    public airports: IAirport[];
    public flights: IFlight[];
    public planes: IPlane[];
    form: FormGroup;
    public errorMessageForAirports: string = '';
    public flightDropDowns: IFlightDropDown[];
    public seats: ISeats[];
    public formSubmitted: boolean = false;
    public locationsChosed: boolean = false;
    public flightChosed: boolean = false;
    public seatChosed: boolean = false;
    public locationValidate: boolean = false;
    public locationValidateMessage: string = '';
    private headers = new Headers();
    private bookingPost: IBookingFrom;
    public name: string;
    public ticketStartingAirportName: string = '';
    public ticketEndAirportName: string;
    public ticket: IBooking;

    getFlights(form: any): void {
        this.flightDropDowns = [];
        this.seats = [];
        this.locationValidate = false;
        if ((form.startingPoint !== null && form.endPoint !== null) &&
            form.startingPoint === form.endPoint) {
            this.locationValidateMessage = "sihtkoht ja läthekoht ei saa olla samad!";
            this.locationValidate = true;
        } else {

            this.http.get(this.baseUrl + '/api/Flight/find/' + form.startingPoint + '/' + form.endPoint).subscribe(
                res => {
                    this.flightDropDowns = res.json() as IFlightDropDown[];
                    if (this.flightDropDowns.length === 0 && (form.startingPoint !== null && form.endPoint !== null)) {
                        this.locationValidate = true;
                        this.locationValidateMessage = "Antud sihtkoha ja lähtekohaga lende hetkel ei leidu";
                        return;
                    } else if ((form.startingPoint !== null && form.endPoint !== null) &&
                        form.startingPoint !== form.endPoint) {
                        this.locationsChosed = true;
                        return;
                    }
                });
        }
    }

    getSeats(form: any): void {
        this.seats = [];
        this.http.get(this.baseUrl + '/api/Planes/findSeats/' + form.time.flightId + '/' + form.time.planeId)
            .subscribe(res => {
                this.seats = res.json() as ISeats[];
                this.seats = this.seats.sort((a, b) => a.seatNumber - b.seatNumber);
                this.flightChosed = true;
            });
    }

    getPersonDetails() {
        if (this.auth.isAuthincated()) {
            this.form.controls["firstName"].disable();
            this.form.controls["lastName"].disable();
            this.form.controls["idCode"].disable();
        } else {
            this.seatChosed = true;
        }
    }

    constructor(private http: Http,
        @Inject('BASE_URL') private baseUrl: string,
        private fb: FormBuilder,
        private auth: AuthService) {
        this.form = fb.group({
            firstName: ["", Validators.compose([Validators.required])],
            lastName: ["", Validators.compose([Validators.required])],
            idCode: ["", Validators.compose([Validators.required, Validators.minLength(10), Validators.pattern("^[0-9]*$")])],
            startingPoint: [null, Validators.compose([Validators.required, Validators.min(1)])],
            endPoint: [null, Validators.compose([Validators.required, Validators.min(1)])],
            time: [0, Validators.compose([Validators.required, Validators.min(1)])],
            seat: [null, Validators.compose([Validators.required, Validators.min(1)])],
            validate: ""
        });
    }

    book(form: any) {
        this.headers.append('Authorization', 'Bearer ' + this.auth.getToken());
        console.log(this.headers);
        console.log('bearer ' + this.auth.getToken());
        var requestOptions = new RequestOptions({ headers: this.headers });
        this.bookingPost = {
            seatId: form.seat,
            flightId: form.time.flightId,
            person: {
                firstName: form.firstName,
                lastName: form.lastName,
                personIdCode: form.idCode
            }
        }
        this.http.post(this.baseUrl + 'api/Bookings/book', this.bookingPost, requestOptions).subscribe(result => {
            this.ticket = result.json().result as IBooking;
            this.formSubmitted = true;
            
        },error => console.log(error));
       console.log(this.ticket);
    }

    ngOnInit() {
        this.http.get(this.baseUrl + 'api/airport').subscribe(result => {
                this.airports = result.json() as IAirport[];
            },
            error => console.error(error));
     
    }

    get firstName() { return this.form.get('firstName'); }

    get lastName() { return this.form.get('lastName'); }

    get idCode() { return this.form.get('idCode'); }

    get seat() { return this.form.get('seat'); }

    get time() {return this.form.get('time');}

    get endPoint() { return this.form.get('endPoint'); }

    get startingPoint() { return this.form.get('startingPoint'); }
}