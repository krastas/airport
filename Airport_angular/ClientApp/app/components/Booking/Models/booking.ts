﻿


export interface IBookingFrom {

    flightId: number;
    seatId: number;
    person: IPerson;
}

export interface IPerson {
    firstName: string;
    lastName: string;
    personIdCode: string;
}

export interface IFlightDropDown {
    flightId: number;
    startingDateTime: Date;
    planeId: number;
}

export interface ISeats {
    seatId: number;
    seatNumber: number;
}

export class IPlane {
    planeId: number = 0;
    planeSeatsNumberId: number = 0;
}

export class IFlight {
    flightId: number = 0;
    airportId: number = 0;
    flightLineId: number = 0;
    planeId: number = 0;
    flightStart: Date;
    flightEnd: Date;
}
export class IAirport {
    airportId: number = 0;
    airportName: string = "";
} 