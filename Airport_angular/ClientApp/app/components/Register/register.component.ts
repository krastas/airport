﻿import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { RegisterUser } from '../../auth/model/registerUser';
import { RegistrationValidator } from './RegistrationValidator';
import { AuthService } from '../../auth/auth.service';


@Component({
    selector: 'register',
    templateUrl: './register.component.html',
    providers: [ AuthService ]
})

export class RegisterComponent {
    public passwordValidate: boolean = false;
    public registerError: boolean = false;

    form: FormGroup;
    passwordFormGroup: FormGroup;
    passPattern = "(?=^.{6,255}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*";


    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, private fb: FormBuilder, private auth: AuthService) {
            
        this.passwordFormGroup = fb.group({
            password: ['', Validators.compose([Validators.required, Validators.pattern(this.passPattern)])],
            confirmPassword: ['', Validators.compose([Validators.required, Validators.pattern(this.passPattern)])]
        }, {
                validator: RegistrationValidator.validate.bind(this)
            });
        this.form = fb.group({
            firstName: ["", Validators.required],
            lastName: ["", Validators.required],
            personId: ["", Validators.compose([Validators.required, Validators.minLength(10), Validators.pattern("^[0-9]*$")])],
            email: ["", Validators.compose([Validators.required, Validators.email])],
            password: this.passwordFormGroup,
            confirmPassword: this.passwordFormGroup,
            validate: ""
        });
    }

    register(form: any) {
        
        var registerUser: RegisterUser = {
            idCode: form.personId,
            firstName: form.firstName,
            lastName: form.lastName,
            email: form.email,
            password: form.password.password,
            confirmPassword: form.password.confirmPassword
        }
        return this.auth.register(registerUser).catch(()=> this.registerError = true);
    }

    get firstName() { return this.form.get('firstName'); }

    get lastName() { return this.form.get('lastName'); }

    get personId() { return this.form.get('personId'); }
    get email() { return this.form.get('email'); }
    get password() { return this.passwordFormGroup.get('password'); }
    get confirmPassword() { return this.passwordFormGroup.get('confirmPassword'); }
}

