﻿import { FormGroup } from '@angular/forms';

export class RegistrationValidator {
    static validate(form: FormGroup) {
        let password = form.controls.password.value;
        let confirmPassword = form.controls.confirmPassword.value;
        let passPattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\.])(?=.{8,})";

        if (confirmPassword.length <= 0) {
            return null;
        }

        if (confirmPassword !== password) {
            return {
                doesMatchPassword: true
            };
        }
        if (!password.match(passPattern)) {
            return {
                faultyPassword: true
            };
        }

        return null;

    }
}