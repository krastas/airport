﻿import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { AuthService } from '../../auth/auth.service'
import { User } from '../../auth/model/User'
import { Router } from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    providers: [AuthService]
})
export class LoginComponent {

    form: FormGroup;
    public loginError: boolean = false;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, private fb: FormBuilder
        , private auth: AuthService, private router: Router) {
        this.form = fb.group({
            email: ["", Validators.compose([Validators.required, Validators.email])],
            password: ["", Validators.compose([Validators.required])],           
            validate: ""
        });        
    }

    login(form: any) {        
        const user: User = {
            email: form.email,
            password: form.password
        }
        if (this.form.controls['email'].valid && this.form.controls['password'].valid) {
            return this.auth.login(user).catch(() => {
                this.loginError = true;
            });
        }
    }

    goRegister() {
        this.router.navigateByUrl('register');
    }

    get email() { return this.form.get('email'); }
    get password() { return this.form.get('password'); }
}
