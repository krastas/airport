﻿import { Component, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { forEach } from '@angular/router/src/utils/collection';
import { MethodCall } from '@angular/compiler';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';



import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';



@Component({
    selector: 'manage',
    templateUrl: './manage.component.html'
})

export class ManageComponent {
    public airports: IAirport[];
    public planes: IPlane[];
    public flights: IFlights[];
    public flightlines: IFlightLines[];
    airportForm: FormGroup;
    planeForm: FormGroup;
    flightLineForm: FormGroup;
    flightForm: FormGroup;
    private headers = new Headers();
    private requestOptions = new RequestOptions();



    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, private fb: FormBuilder, private auth: AuthService) {
        this.headers.append('Authorization', 'Bearer ' + this.auth.getToken());
        this.requestOptions.headers = this.headers;
        this.reset();
    }

    reset(): void {      
        this.http.get(this.baseUrl + 'api/Airport/All', this.requestOptions).subscribe(result => {
            this.airports = result.json() as IAirport[];

        },
            error => console.error(error));

        this.http.get(this.baseUrl + 'api/Planes/All', this.requestOptions).subscribe(result => {
            this.planes = result.json() as IPlane[];

        },
            error => console.error(error));
        this.http.get(this.baseUrl + 'api/Flight/All', this.requestOptions).subscribe(result => {
            console.log(result.json())
            this.flights = result.json() as IFlights[];

        },
            error => console.error(error));
        this.http.get(this.baseUrl + 'api/FlightLine/All', this.requestOptions).subscribe(result => {
            this.flightlines = result.json() as IFlightLines[];

        },
            error => console.error(error));

        this.airportForm = this.fb.group({
            airportName: ["", Validators.required],
            airportLocation: ["", Validators.required],
            validate: ""
        })

        this.planeForm = this.fb.group({
            planeType: ["", Validators.required],
            planeMark: ["", Validators.required],
            planeSeats: ["", Validators.required],
            validate: ""
        })
        this.flightLineForm = this.fb.group({
            flightLineStart: ["", Validators.required],
            flightLineEnd: ["", Validators.required],
            airport1: ["", Validators.required],
            airport2: ["", Validators.required],
            validate: ""
        })
        this.flightForm = this.fb.group({
            flightLine: ["", Validators.required],
            flightPlane: ["", Validators.required],
            flightStart: ["", Validators.required],
            flightEnd: ["", Validators.required],
            validate: ""
        })



    }

    deleteAirport(id: number) {
    
        return this.http
            .delete(this.baseUrl + 'api/Airport/' + id, this.requestOptions).subscribe(() => this.reset());
    }
    deletePlane(id: number) {
       
        return this.http
            .delete(this.baseUrl + 'api/Planes/' + id, this.requestOptions).subscribe(() => this.reset());
    }
    deleteFlightLine(id: number) {
       
        return this.http
            .delete(this.baseUrl + 'api/FlightLine/' + id, this.requestOptions).subscribe(() => this.reset());
    }
    deleteFlight(id: number) {
      
        return this.http
            .delete(this.baseUrl + 'api/Flight/' + id, this.requestOptions).subscribe(() => this.reset());
    }
    addAirport(form: any): any {
        
        var airport: IAirport = {
            airportId: 0,
            airportName: form.airportName,
            location: form.airportLocation
        };
        return this.http
            .post(this.baseUrl + 'api/Airport/Add', airport, this.requestOptions).subscribe(() => this.reset());
    }

    AddPlane(form: any): any {
        
        var plane: IPlane = {
            planeId: 0,
            planeType: form.planeType,
            planeMark: form.planeMark,
            planeSeatsNumber: form.planeSeats
        };
        return this.http
            .post(this.baseUrl + 'api/Planes/Add', plane, this.requestOptions).subscribe(() => this.reset());
    }
    AddFlightLine(form: any): any {
       
        var flightLine: IFlightLines = {
            flightLineId: 0,
            airport1Id: form.airport1,
            airport2Id: form.airport2,
            flightLineStart: form.flightLineStart,
            flightLineEnd: form.flightLineEnd
        };

        return this.http
            .post(this.baseUrl + 'api/FlightLine/Add', flightLine, this.requestOptions).subscribe(() => this.reset());
    }
    AddFlight(form: any): any {
       
        var flight: IFlights = {
            flightId: 0,
            flightLineId: form.flightLine,
            planeId: form.flightPlane,
            flightStart: form.flightStart,
            flightEnd: form.flightEnd
        };
        console.log(flight);
        return this.http
            .post(this.baseUrl + 'api/Flight/Add', flight, this.requestOptions).subscribe(() => this.reset());
    }



}

interface IAirport {
    airportId: number;
    airportName: string;
    location: string;
}

interface IPlane {
    planeId: number;
    planeMark: string;
    planeType: string;
    planeSeatsNumber: number;
}


interface IFlights {
    flightId: number;
    flightLineId: number;
    planeId: number;
    flightStart: Date;
    flightEnd: Date;
}

interface IFlightLines {
    flightLineId: number;
    flightLineStart: Date;
    flightLineEnd: Date;
    airport1Id: number;
    airport2Id: number;

}