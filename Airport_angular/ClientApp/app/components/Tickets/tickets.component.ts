﻿import { Component, Inject} from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { forEach } from '@angular/router/src/utils/collection';
import { MethodCall } from '@angular/compiler';
import { AuthService } from '../../auth/auth.service';
import * as Booking from './Model/model';
import IBooking = Booking.IBooking;


@Component({
    selector: 'tickets',
    templateUrl: './tickets.component.html'
})
export class TicketsComponent {
    public validbookings: IBooking[];
    public notvalidbookings: IBooking[];
    private headers = new Headers();
    public requestOptions = new RequestOptions();

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, private auth: AuthService) {
        this.headers.append('Authorization', 'Bearer ' + this.auth.getToken());
        this.requestOptions.headers = this.headers;
        this.reset();
        
      
    }
    reset(): void {   
        this.http.get(this.baseUrl + 'api/Bookings/findvalid', this.requestOptions).subscribe(result => {
            this.validbookings = result.json() as IBooking[];

        },
            error => console.error(error));

        this.http.get(this.baseUrl + 'api/Bookings/findnotvalid', this.requestOptions).subscribe(result => {
            this.notvalidbookings = result.json() as IBooking[];

        },
            error => console.error(error));
    }
    deleteTicket(id: number) {
       
        return this.http
            .delete(this.baseUrl + 'api/Bookings/' + id, this.requestOptions).subscribe(() => {
                this.reset();
            });
    }
}




