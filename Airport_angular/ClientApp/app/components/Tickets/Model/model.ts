﻿export interface IBooking {
    bookingId: number;
    personFirstName: string;
    personLastName: string;
    airport1: string;
    airport2: string;
    flightStart: Date;
    seatNumber: number;
} 