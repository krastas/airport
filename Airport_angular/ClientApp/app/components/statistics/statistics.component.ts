﻿import { Component, Inject, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { forEach } from '@angular/router/src/utils/collection';
import { MethodCall } from '@angular/compiler';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';



import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';



@Component({
    selector: 'statistics',
    templateUrl: './statistics.component.html'
})

export class StatisticsComponent  implements OnInit {
    ngOnInit(): void {
        this.headers.append('Authorization', 'Bearer ' + this.auth.getToken());
        this.requestOptions.headers = this.headers;
        this.http.get(this.baseUrl + 'api/flight/findold', this.requestOptions).subscribe(result => {
                this.flights = result.json().length;
            },
            error => console.error(error));
        this.seats = [];
        this.http.get(this.baseUrl + '/api/Planes/findSeat/', this.requestOptions)
            .subscribe(res => {
                this.seats = res.json().length;
            });
        this.http.get(this.baseUrl + '/api/Airport/All', this.requestOptions)
            .subscribe(res => {
                this.airports = res.json().length;
            });
        this.http.get(this.baseUrl + 'api/Planes/All', this.requestOptions).subscribe(result => {
            this.planes = result.json().length;

        });
        this.http.get(this.baseUrl + 'api/Bookings/All', this.requestOptions).subscribe(result => {
            this.bookings = result.json().length;
        }); }

    public airports: IAirport[];
    public flights: IFlight[];
    public flights2: IFlight[];
    public planes: IPlane[];
    form: FormGroup;
    public errorMessageForAirports: string = '';
    public flightDropDowns: IFlightDropDown[];
    public seats: ISeats[];
    public bookings: IBooking[];
    public formSubmitted: boolean = false;
    public locationsChosed: boolean = false;
    public flightChosed: boolean = false;
    public seatChosed: boolean = false;
    public locationValidate: boolean = false;
    private headers = new Headers();
    private requestOptions = new RequestOptions();


    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, private fb: FormBuilder, private auth: AuthService) {
        
    }

        
     
}
export interface IBookingFrom {

    flightId: number;
    seatId: number;
    person: IPerson;
}

export interface IPerson {
    firstName: string;
    lastName: string;
    personIdCode: string;
}

export interface IFlightDropDown {
    flightId: number;
    startingDateTime: Date;
    planeId: number;
}

export interface ISeats {
    seatId: number;
    seatNumber: number;
}
export class IPlane {
    planeId: number = 0;
    planeSeatsNumberId: number = 0;
}

export class IFlight {
    flightId: number = 0;
    airportId: number = 0;
    flightLineId: number = 0;
    planeId: number = 0;
    flightStart: Date;
    flightEnd: Date;
}
export class IAirport {
    airportId: number = 0;
    airportName: string = "";
} 
interface IBooking {
    bookingId: number;
    personFirstName: string;
    personLastName: string;
    airport1: string;
    airport2: string;
    flightStart: Date;
    seatNumber: number;
} 
