import { Component } from '@angular/core';
import { AuthService} from '../../auth/auth.service';
import { Router } from '@angular/router';


@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css'],
    providers: [AuthService]
})
export class NavMenuComponent {
    public authencated: boolean = false;


    constructor(public auth: AuthService, private router: Router) {
      
    }

    logout() {
        this.auth.logout();
        this.router.navigateByUrl('/home');
    }
}
