﻿using BL.Dto;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Factories
{
    public interface IFlightLineFactory
    {
        FlightLineDto Transform(FlightLine flightLine);
    }
    public class FlightLineFactory : IFlightLineFactory
    {
        public FlightLineDto Transform(FlightLine flightLine)
        {
            return new FlightLineDto()
            {
                FlightLineId = flightLine.FlightLineId,
                FlightLineStart = flightLine.FlightLineStart,
                FlightLineEnd = flightLine.FlightLineEnd,
                Airport1 = flightLine.Airport1.AirportName,
                Airport2 = flightLine.Airport2.AirportName
            };
        }
    }
}
