﻿using BL.Dto;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Factories
{
    public interface IBookingFactory
    {
        BookingDto Transform(Booking booking);
        SeatDtoForBooking TranformToSeatForBookingDto(Seat seat);
    }
    public class BookingFactory : IBookingFactory
    {
        public BookingDto Transform(Booking booking)
        {
            return new BookingDto()
            {
                BookingId = booking.BookingId,
                PersonFirstName = booking.Person.FirstName,
                PersonLastName = booking.Person.LastName,
                Airport1 = booking.Flight.FlightLine.Airport1.AirportName,
                Airport2 = booking.Flight.FlightLine.Airport2.AirportName,
                FlightStart = booking.Flight.FlightStart,
                SeatNumber = booking.Seat.SeatNumber

            };
        }

        public SeatDtoForBooking TranformToSeatForBookingDto(Seat seat)
        {
            return new SeatDtoForBooking()
            {
                SeatId = seat.SeatId,
                SeatNumber = seat.SeatNumber
                
            };
        }
    }
}
