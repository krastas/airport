﻿using BL.Dto;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;


namespace BL.Factories
{
    public interface IFlightsFactory
    {
        FlightsDto Transform(Flight flight);

        BookingFlightDetailsDto TransformToDto(Flight flight);
    }

    public class FlightsFactory: IFlightsFactory
    {
        public FlightsDto Transform(Flight flight)
        {
            return new FlightsDto()
            {
                FlightId = flight.FlightId,
                FlightStart = flight.FlightStart,
                FlightEnd = flight.FlightEnd,
                FlightLineId = flight.FlightLineId,
                FlightLineStart = flight.FlightLine.Airport1.AirportName,
                FlightLineEnd = flight.FlightLine.Airport2.AirportName,
                PlaneMark = flight.Plane.PlaneMark
            };
        }

        public BookingFlightDetailsDto TransformToDto(Flight flight)
        {
            return new BookingFlightDetailsDto()
            {
                FlightId = flight.FlightId,
                StartingDateTime = flight.FlightStart,
                PlaneId = flight.PlaneId
            };
        }
    }
}
