﻿using System;
using System.Collections.Generic;
using System.Text;
using BL.Dto;
using Domain;

namespace BL.Factories
{
    public interface ILocationFactory
    {
        LocationsDto Transform(Airport airport);
    }

    public  class LocationFactory: ILocationFactory
    {
        public LocationsDto Transform(Airport airport)
        {
            return new LocationsDto()
            {
                AirportId = airport.AirportId,
                AirportName = airport.AirportName
            };
        }
    }
}
