﻿using BL.Dto;
using BL.Factories;
using DAL.App.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public interface IBookingService
    {
        Task<List<BookingDto>> GetAllBookingsWithAirportsAsync();
        Task<List<BookingDto>> GetAllValidBookingsWithAirportsAsync(int personId, bool isAdmin);
        Task<List<BookingDto>> GetAllNotValidBookingsWithAirportsAsync(int personId, bool isAdmin);
        Task<List<SeatDtoForBooking>> GetListOfSeatsForBookingAsync(int flightId, int planeId);
        Task<List<SeatDtoForBooking>> GetListOfAllSeatsForBookingAsync();
        Task<BookingDto> GetAddedBookingAsBookingDtoAsync(int bookingId);
    }
    public class BookingService : IBookingService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IBookingFactory _factory;
        public BookingService(IBookingFactory factory, IAppUnitOfWork uow)
        {
            _factory = factory;
            _uow = uow;
        }
        public async Task<List<BookingDto>> GetAllBookingsWithAirportsAsync()
        {
            var bookings = await _uow.Bookings.AllBookingsWithAirportsAsync();
            return new List<BookingDto>(bookings.Select(f => _factory.Transform(f)));
        }

        public async Task<List<BookingDto>> GetAllValidBookingsWithAirportsAsync(int personId, bool isAdmin)
        {
            var bookings = await _uow.Bookings.AllValidBookingsWithAirportsAsync(personId, isAdmin);
            return new List<BookingDto>(bookings.Select(f => _factory.Transform(f)));
        }

        public async Task<List<BookingDto>> GetAllNotValidBookingsWithAirportsAsync(int personId, bool isAdmin)
        {

            var bookings = await _uow.Bookings.AllNotValidBookingsWithAirportsAsync( personId, isAdmin);
            return new List<BookingDto>(bookings.Select(f => _factory.Transform(f)));
        }

        public async Task<List<SeatDtoForBooking>> GetListOfSeatsForBookingAsync(int flightId, int planeId)
        {
            var seats = await _uow.Seats.GetAllAvailableSeatsForPlaneAsync(flightId, planeId);
            return new List<SeatDtoForBooking>(seats.Select(s => _factory.TranformToSeatForBookingDto(s)));
        }
        public async Task<List<SeatDtoForBooking>> GetListOfAllSeatsForBookingAsync()
        {
            var seats = await _uow.Seats.GetListOffAllSeatsOfPlaneAsync();
            return new List<SeatDtoForBooking>(seats.Select(s => _factory.TranformToSeatForBookingDto(s)));
        }

        public async Task<BookingDto> GetAddedBookingAsBookingDtoAsync(int bookingId)
        {
            var booking = await _uow.Bookings.FindBookingByIdWithIncludeAsync(bookingId);
            return _factory.Transform(booking);
        }
    }
}
