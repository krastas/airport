﻿using BL.Dto;
using BL.Factories;
using DAL.App.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public interface IFlightsService
    {
        Task<List<FlightsDto>> GetAllFlightsWithAirportsAsync();
        Task<List<BookingFlightDetailsDto>> GetAllFlightsWithTwoAirportsAsync(int airport1Id, int ariport2Id);
        Task<List<BookingFlightDetailsDto>> GetAllOldFlightsAsync();
    }
    public class FlightsService : IFlightsService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IFlightsFactory _factory;

        public FlightsService(IFlightsFactory factory, IAppUnitOfWork uow)
        {
            _factory = factory;
            _uow = uow;
        }

        public async Task<List<FlightsDto>> GetAllFlightsWithAirportsAsync()
        {
            var flights = await _uow.Flights.AllFlightsWithAirportsAsync();
            return new List<FlightsDto>(flights.Select(f => _factory.Transform(f)));
        }

        public async Task<List<BookingFlightDetailsDto>> GetAllFlightsWithTwoAirportsAsync(int airport1Id, int ariport2Id)
        {
            var flights = await _uow.Flights.FindflightsWith2AirportsAsync(airport1Id, ariport2Id);
            return new List<BookingFlightDetailsDto>(flights.Select(f => _factory.TransformToDto(f)));
        }
        public async Task<List<BookingFlightDetailsDto>> GetAllOldFlightsAsync()
        {
            var flights = await _uow.Flights.FindoldflightsAsync();
            return new List<BookingFlightDetailsDto>(flights.Select(f => _factory.TransformToDto(f)));
        }
    }
}
