﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using BL.Dto;
using BL.Factories;
using DAL.App.Interfaces;
using Domain;

namespace BL.Services
{
    public interface ILocationService
    {
        Task<List<LocationsDto>> GetAllAirportLocations();
        
    }
    public class LocationService: ILocationService
   {
       private readonly IAppUnitOfWork _uow;
       private readonly ILocationFactory _factory;

        public LocationService(ILocationFactory factory, IAppUnitOfWork uow)
        {
            _factory = factory;
            _uow = uow;
        }
        public async Task<List<LocationsDto>> GetAllAirportLocations()
        {
            var airports = new List<Airport>( await _uow.Airports.AllAsync());
            return  airports.Select(c => _factory.Transform(c)).ToList();

        }
    }
}
