﻿using BL.Dto;
using BL.Factories;
using DAL.App.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public interface IFlightLineService
    {
        Task<List<FlightLineDto>> GetAllFlightLinesWithAirportsAsync();
    }
    public class FlightLineService : IFlightLineService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IFlightLineFactory _factory;

        public FlightLineService(IFlightLineFactory factory, IAppUnitOfWork uow)
        {
            _factory = factory;
            _uow = uow;
        }
        public async Task<List<FlightLineDto>> GetAllFlightLinesWithAirportsAsync()
        {
            var flightLines = await _uow.FlightLines.AllFlightLinesWithAirportsAsync();
            return new List<FlightLineDto>(flightLines.Select(f => _factory.Transform(f)));
        }
    }
}
