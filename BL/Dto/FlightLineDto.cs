﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Dto
{
    public class FlightLineDto
    {
        public int FlightLineId { get; set; }
        public DateTime FlightLineStart { get; set; }
        public DateTime FlightLineEnd { get; set; }
        public string Airport1 { get; set; }
        public string Airport2 { get; set; }
    }
}
