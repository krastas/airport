﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Dto
{
    public class BookingDto
    {
        public int BookingId { get; set; }
        public string PersonFirstName { get; set; }
        public string PersonLastName { get; set; }
        public string Airport1 { get; set; }
        public string Airport2 { get; set; }
        public DateTime FlightStart { get; set; }
        public int SeatNumber { get; set; }
    }
}
