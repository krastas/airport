﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Domain;

namespace BL.Dto
{
   public class LocationsDto
    {
        public int AirportId { get; set; }
        [Required]
        public string AirportName { get; set; }
              
     
    }
}
