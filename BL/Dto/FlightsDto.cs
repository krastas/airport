﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Dto
{
    public class FlightsDto
    {
        public int FlightId { get; set; }
        public DateTime FlightStart { get; set; }
        public DateTime FlightEnd { get; set; }
        public int FlightLineId { get; set; }
        public string FlightLineStart { get; set; }
        public string FlightLineEnd { get; set; }
        public string PlaneMark { get; set; }
    }
}
