﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Dto
{
   public class BookingFlightDetailsDto
    {
        public int FlightId { get; set; }

        public int PlaneId { get; set; }

        public DateTime StartingDateTime { get; set; }
    }
}
