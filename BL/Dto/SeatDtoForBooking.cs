﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Dto
{
    public class SeatDtoForBooking
    {

        public int SeatId { get; set; }

        public int SeatNumber { get; set; }
    }
}