﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace DAL.App.EF
{
    public class ApplicationDbContext : IdentityDbContext<UserAccount>, IDataContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Person> People { get; set; }
        public DbSet<Airport> Airports { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Flight> Flights { get; set; }
        public DbSet<FlightLine> FlightLines  { get; set; }
        public DbSet<PersonInSeat> PersonInSeats  { get; set; }
        public DbSet<Plane> Planes  { get; set; }
        public DbSet<PlaneInAirport> PlaneInAirports  { get; set; }
        public DbSet<Seat> Seats  { get; set; }
    protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach(var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys())){
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            


            base.OnModelCreating(builder);
        }
    }
}
