﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFBookingRepository : EFRepository<Booking>, IBookingRepository
    {
        public EFBookingRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public async Task<List<Booking>> AllBookingsWithAirportsAsync()
        {
            return await RepositoryDbSet.Include(f => f.Flight).ThenInclude(f => f.FlightLine).ThenInclude(f => f.Airport1).Include(f => f.Flight).ThenInclude(f => f.FlightLine).ThenInclude(f => f.Airport2).Include(f => f.Person).Include(f => f.Seat).ToListAsync();
        }

        public async Task<List<Booking>> FindBookingsById(int id)
        {
            return await RepositoryDbSet.Where(m => m.Flight.FlightLine.Airport1.AirportId == id).ToListAsync();
        }
        public async Task<bool> BookingExists(int id)
        {
            return await RepositoryDbSet.Where(e => e.BookingId == id).AnyAsync();
        }

        public async Task<List<Booking>> AllValidBookingsWithAirportsAsync(int personId, bool isAdmin)
        {
            if (isAdmin)
            {
                return await RepositoryDbSet.Where(b=>b.Flight.FlightStart > DateTime.Now).Include(f => f.Flight).ThenInclude(f => f.FlightLine).ThenInclude(f => f.Airport1).Include(f => f.Flight).ThenInclude(f => f.FlightLine).ThenInclude(f => f.Airport2).Include(f => f.Person).Include(f => f.Seat).ToListAsync();
            }
            else
            {
                return await RepositoryDbSet.Where(b => b.Flight.FlightStart > DateTime.Now && b.PersonId == personId).Include(f => f.Flight).ThenInclude(f => f.FlightLine).ThenInclude(f => f.Airport1).Include(f => f.Flight).ThenInclude(f => f.FlightLine).ThenInclude(f => f.Airport2).Include(f => f.Person).Include(f => f.Seat).ToListAsync();
            }
        }


        public async Task<List<Booking>> AllNotValidBookingsWithAirportsAsync(int personId,  bool isAdmin)
        {
            if (isAdmin)
            {
                return await RepositoryDbSet.Where(b => b.Flight.FlightStart < DateTime.Now).Include(f => f.Flight).ThenInclude(f => f.FlightLine).ThenInclude(f => f.Airport1).Include(f => f.Flight).ThenInclude(f => f.FlightLine).ThenInclude(f => f.Airport2).Include(f => f.Person).Include(f => f.Seat).ToListAsync();
            } else { 
            return await RepositoryDbSet.Where(b => b.Flight.FlightStart < DateTime.Now &&  b.PersonId == personId ).Include(f => f.Flight).ThenInclude(f => f.FlightLine).ThenInclude(f => f.Airport1).Include(f => f.Flight).ThenInclude(f => f.FlightLine).ThenInclude(f => f.Airport2).Include(f => f.Person).Include(f => f.Seat).ToListAsync();
            }
        }

        public async Task<Booking> FindBookingByIdWithIncludeAsync(int bookingId)
        {
            return await RepositoryDbSet.Where(b => b.BookingId == bookingId).Include(f => f.Flight).ThenInclude(f => f.FlightLine).
                ThenInclude(f => f.Airport1).Include(f => f.Flight).ThenInclude(f => f.FlightLine).
                ThenInclude(f => f.Airport2).Include(f => f.Person).Include(f => f.Seat).FirstOrDefaultAsync();
        }
    }
}
