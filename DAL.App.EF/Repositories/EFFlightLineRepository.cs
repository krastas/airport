﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFFlightLineRepository : EFRepository<FlightLine>, IFlightLineRepository
    {
        public EFFlightLineRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public async Task<List<FlightLine>> AllFlightLinesWithAirportsAsync()
        {
            return await RepositoryDbSet.Include(f => f.Airport1).Include(f => f.Airport2).ToListAsync();

        }

        

        public async Task<List<FlightLine>> FindFlightLinesById(int id)
        {
            return await RepositoryDbSet.Where(m => m.Airport1.AirportId == id).Where(m => m.Airport2.AirportId == id).ToListAsync();
        }
    }
}
