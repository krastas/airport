﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFSeatRepository : EFRepository<Seat>, ISeatRepository
    {
        public EFSeatRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public async Task<List<Seat>> GetAllAvailableSeatsForPlaneAsync(int flightId, int planeId)
        {
            return await RepositoryDbSet
                .Where(s => s.PlaneId == planeId && s.PeopleinSeats.All(pis => pis.FlightId != flightId)).ToListAsync();
        }
        public async Task<List<Seat>> GetListOffAllSeatsOfPlaneAsync()
        {
            return await RepositoryDbSet
                .Where(s => s.PeopleinSeats.All(pis => pis.Flight.FlightStart > DateTime.Now)).ToListAsync();
        }

        public async Task<List<Seat>> GetListOffAllSeatsOfPlaneAsync(int planeId)
        {
            return await RepositoryDbSet
               .Where(s => s.PlaneId == planeId).ToListAsync();
        }
    }
}
