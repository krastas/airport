﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.App.EF.Repositories
{
    public class EFPlaneRepository : EFRepository<Plane>, IPlaneRepository
    {
        public EFPlaneRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public List<Plane> FindPlanesById(int id)
        {
            return RepositoryDbSet.Where(m => m.PlaneId == id).ToList();
        }
    }
}
