﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFFlightRepository : EFRepository<Flight>, IFlightRepository
    {
        public EFFlightRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public async Task<List<Flight>> AllFlightsWithAirportsAsync()
        {
            return await RepositoryDbSet.Include(f => f.Plane).Include(f => f.FlightLine).ThenInclude(f => f.Airport1).Include(f => f.FlightLine).ThenInclude(f => f.Airport2).ToListAsync();
        }

        public async Task<List<Flight>> FindflightsWith2AirportsAsync(int airport1Id, int airport2Id)
        {
            var a = await RepositoryDbSet.Where(f =>
                f.FlightStart > DateTime.Now && f.FlightLine.Airport1Id == airport1Id &&
                f.FlightLine.Airport2Id == airport2Id).ToListAsync();

            return a;

        }
        public async Task<List<Flight>> FindoldflightsAsync()
        {
            var a = await RepositoryDbSet.Where(f =>
                f.FlightStart < DateTime.Now).ToListAsync();

            return a;

        }

    }
}
