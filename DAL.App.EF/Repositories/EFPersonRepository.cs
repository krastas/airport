﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.App.EF.Repositories
{
    public class EFPersonRepository : EFRepository<Person>, IPersonRepository
    {
        public EFPersonRepository(DbContext dataContext) : base(dataContext)
        {
        }

        public async Task<Person> IfExsistsAsync(Person person)
        {
           var foundPerson = await RepositoryDbSet
                .Where(p => p.FirstName == person.FirstName && p.LastName == person.LastName &&
                            p.PersonIdCode == person.PersonIdCode).FirstOrDefaultAsync();
            return foundPerson;
        }
    }
}
