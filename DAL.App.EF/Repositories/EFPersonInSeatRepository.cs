﻿using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;

namespace DAL.App.EF.Repositories
{
    public class EFPersonInSeatRepository : EFRepository<PersonInSeat>, IPersonInSeatRepository
    {
        public EFPersonInSeatRepository(DbContext dataContext) : base(dataContext)
        {
        }
        public async Task<PersonInSeat> FindPersonInSeatBySeatIdAsync(int seatId)
        {
            return await RepositoryDbSet.Where(p=>p.SeatId==seatId).FirstOrDefaultAsync();
        }
    }
}
