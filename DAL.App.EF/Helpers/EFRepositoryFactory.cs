﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.App.EF.Repositories;
using DAL.App.Interfaces.Helpers;
using DAL.App.Interfaces.Repositories;
using DAL.EF.Repositories;
using DAL.Interfaces;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Helpers
{
    public class EFRepositoryFactory : IRepositoryFactory
    {
        private readonly Dictionary<Type, Func<IDataContext, object>> _customRepositoryFactories
            = GetCustomRepoFactories();

        private static Dictionary<Type, Func<IDataContext, object>> GetCustomRepoFactories()
        {
            return new Dictionary<Type, Func<IDataContext, object>>()
            {
               
                {typeof(IAirportRepository), (dataContext) => new EFAirportRepository(dataContext as ApplicationDbContext) },
                {typeof(IBookingRepository), (dataContext) => new EFBookingRepository(dataContext as ApplicationDbContext) },
                {typeof(IFlightRepository), (dataContext) => new EFFlightRepository(dataContext as ApplicationDbContext) },
                {typeof(IFlightLineRepository), (dataContext) => new EFFlightLineRepository(dataContext as ApplicationDbContext) },
                {typeof(IPersonRepository), (dataContext) => new EFPersonRepository(dataContext as ApplicationDbContext) },
                {typeof(IPersonInSeatRepository), (dataContext) => new EFPersonInSeatRepository(dataContext as ApplicationDbContext) },
                {typeof(IPlaneRepository), (dataContext) => new EFPlaneRepository(dataContext as ApplicationDbContext) },
                {typeof(IPlaneInAirportRepository), (dataContext) => new EFPlaneInAirportRepository(dataContext as ApplicationDbContext) },
                {typeof(ISeatRepository), (dataContext) => new EFSeatRepository(dataContext as ApplicationDbContext) },
            };
        }

        public Func<IDataContext, object> GetCustomRepositoryFactory<TRepoInterface>() where TRepoInterface : class
        {
            _customRepositoryFactories.TryGetValue(
                typeof(TRepoInterface),
                out Func<IDataContext, object> factory
            );
            return factory;
        }

        public Func<IDataContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class
        {

            return (dataContext) => new EFRepository<TEntity>(dataContext as ApplicationDbContext);
        }
    }
}
