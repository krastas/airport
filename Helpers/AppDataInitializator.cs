﻿using System;
using System.Linq;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Identity;

namespace Helpers
{
    public class AppDataInitializator
    {
        public static void InitializeAppDatabase(ApplicationDbContext context)
        {
            if (!context.People.Any())
            {
                context.People.Add(new Person()
                {
                    FirstName = "Test",
                    LastName = "Olen",
                    PersonIdCode = "39623459212"
                });
                context.People.Add(new Person()
                {
                    FirstName = "tester",
                    LastName = "olen123",
                    PersonIdCode = "3988888888",
                });
                context.SaveChanges();
            }

            if (!context.Airports.Any())
            {
                context.Airports.Add(new Airport
                {
                    AirportName = "Tallinna airport",
                    Location = "Estonia"
                });
                context.Airports.Add(new Airport
                {
                    AirportName = "Munich airport",
                    Location = "Germany"
                });
                context.Airports.Add(new Airport
                {
                    AirportName = "Moscow airport",
                    Location = "Russia"
                });
                context.SaveChanges();
            }

            if (!context.Planes.Any())
            {
                context.Planes.Add(new Plane
                {
                    PlaneMark = "Boeing 505",
                    PlaneSeatsNumber = 50,
                    PlaneType = "BIG PlANE"
                });
                context.Planes.Add(new Plane
                {
                    PlaneMark = "Boeing 400",
                    PlaneSeatsNumber = 70,
                    PlaneType = "BIG PlANE"
                });
                context.Planes.Add(new Plane
                {
                    PlaneMark = "Boeing 200",
                    PlaneSeatsNumber = 30,
                    PlaneType = "MEDIUM PlANE"
                });
                context.SaveChanges();
            }

            if (!context.Seats.Where(p => p.PlaneId == 1).Any())
            {
                for (var i = 1;
                    i < context.Planes.Where(p => p.PlaneId == 1).FirstOrDefault().PlaneSeatsNumber + 1;
                    i++)
                {
                    context.Seats.Add(new Seat()
                    {
                        PlaneId = 1,
                        SeatNumber = i
                    });
                }

                context.SaveChanges();
            }

            if (!context.Seats.Where(p => p.PlaneId == 2).Any())
            {
                for (var i = 1;
                    i < context.Planes.Where(p => p.PlaneId == 2).FirstOrDefault().PlaneSeatsNumber + 1;
                    i++)
                {
                    context.Seats.Add(new Seat()
                    {
                        PlaneId = 2,
                        SeatNumber = i
                    });
                }

                context.SaveChanges();
            }

            if (!context.Seats.Where(p => p.PlaneId == 3).Any())
            {
                for (var i = 1;
                    i < context.Planes.Where(p => p.PlaneId == 3).FirstOrDefault().PlaneSeatsNumber + 1;
                    i++)
                {
                    context.Seats.Add(new Seat()
                    {
                        PlaneId = 3,
                        SeatNumber = i
                    });
                }

                context.SaveChanges();
            }


            if (!context.FlightLines.Any())
            {
                context.FlightLines.Add(new FlightLine
                {
                    Airport1Id = 3,
                    Airport2Id = 2,
                    FlightLineStart = new DateTime(12, 01, 16),
                    FlightLineEnd = new DateTime(3000, 01, 01)
                });
                context.FlightLines.Add(new FlightLine
                {
                    Airport1Id = 1,
                    Airport2Id = 2,
                    FlightLineStart = new DateTime(1999, 01, 01),
                    FlightLineEnd = new DateTime(3000, 01, 01)
                });
                context.FlightLines.Add(new FlightLine
                {
                    Airport1Id = 1,
                    Airport2Id = 3,
                    FlightLineStart = new DateTime(1999, 01, 01),
                    FlightLineEnd = new DateTime(3000, 01, 01)
                });
                context.SaveChanges();
            }

            if (!context.Flights.Any())
            {
                context.Flights.Add(new Flight
                {
                    FlightLineId = 2,
                    PlaneId = 3,
                    FlightStart = new DateTime(2018, 01, 01, 7, 0, 0),
                    FlightEnd = new DateTime(2018, 01, 02, 10, 50, 0)
                });
                context.Flights.Add(new Flight
                {
                    FlightLineId = 2,
                    PlaneId = 2,
                    FlightStart = new DateTime(2019, 01, 01, 7, 0, 0),
                    FlightEnd = new DateTime(2019, 01, 01, 10, 50, 0)
                });
                context.Flights.Add(new Flight
                {
                    FlightLineId = 1,
                    PlaneId = 1,
                    FlightStart = new DateTime(2017, 11, 11, 3, 0, 0),
                    FlightEnd = new DateTime(2017, 12, 01, 7, 0, 0)
                });
                context.SaveChanges();
            }

            if (!context.PlaneInAirports.Any())
            {
                context.PlaneInAirports.Add(new PlaneInAirport
                {
                    AirportId = 3,
                    PlaneId = 1,
                });
                context.PlaneInAirports.Add(new PlaneInAirport
                {
                    AirportId = 2,
                    PlaneId = 2,
                });
                context.SaveChanges();
            }

            if (!context.Bookings.Any())
            {
                context.Bookings.Add(new Booking
                {
                    SeatId = 30,
                    PersonId = 2,
                    FlightId = 2,
                });
                context.Bookings.Add(new Booking
                {
                    SeatId = 15,
                    PersonId = 1,
                    FlightId = 2,
                });
                context.Bookings.Add(new Booking
                {
                    SeatId = 35,
                    PersonId = 2,
                    FlightId = 3,
                });

                context.SaveChanges();
            }

            if (!context.PersonInSeats.Any())
            {
                context.PersonInSeats.Add(new PersonInSeat
                {
                    SeatId = 30,
                    PersonId = 2,
                    FlightId = 2,
                });

                context.SaveChanges();
            }


            context.SaveChanges();
        }

        private static readonly string[] Roles = new[]
        {
            "User",
            "Admin"
        };

        public static void InitializeIdentity(UserManager<UserAccount> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            foreach (var role in Roles)
            {
                if (!roleManager.RoleExistsAsync(role).Result)
                {
                    roleManager.CreateAsync(new IdentityRole(role)).Wait();
                }
            }

            var userName = "admin@eesti.ee";
            var userPass = "Foobar.foobar1";

            if (userManager.FindByNameAsync(userName).Result == null)
            {
                var adminUser = new UserAccount()
                {
                    UserName = userName,
                    Email = userName,
                    FirstName = "Admin",
                    LastName = "Admin",
                    PersonId = 1,
                    Since = DateTime.Now
                };

                var res = userManager.CreateAsync(adminUser, userPass).Result;
                if (res == IdentityResult.Success)
                {
                    foreach (var role in Roles)
                    {
                        userManager.AddToRoleAsync(adminUser, role).Wait();
                    }
                }

                var user = new UserAccount()
                {
                    UserName = "a@a.ee",
                    Email = "a@a.ee",
                    FirstName = "a",
                    LastName = "v",
                    Since = DateTime.Now,
                    PersonId = 2
                };

                var result = userManager.CreateAsync(user, "Kalamaja.1").Result;
                if (result == IdentityResult.Success)
                {
                   
                        userManager.AddToRoleAsync(user, "User").Wait();
                    
                }
            }
        }
    }
}