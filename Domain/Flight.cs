﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Flight
    {
        public int FlightId { get; set; }

        public int FlightLineId { get; set; }
        public FlightLine FlightLine { get; set; }
        public int PlaneId { get; set; }
        public Plane Plane { get; set; }
        public DateTime FlightStart { get; set; } = new DateTime();
        public DateTime FlightEnd { get; set; } = new DateTime();
    }
}
