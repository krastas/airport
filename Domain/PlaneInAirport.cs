﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class PlaneInAirport
    {
        public int PlaneInAirportId { get; set; }
        public int AirportId { get; set; }
        public Airport Airport { get; set; }
        public int PlaneId { get; set; }
        public Plane Plane { get; set; }


    }
}
