﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class UserAccount : IdentityUser
    {
        public DateTime Since { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FirstLast => FirstName + " " + LastName;

        public int PersonId  { get; set; }

        public Person Person { get; set; }
    }
}
