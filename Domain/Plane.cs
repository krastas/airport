﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Plane
    {
        public int PlaneId { get; set; }
        [Required]
        [MaxLength(128)]
        public string PlaneMark { get; set; }
       
        [Required]
        public int PlaneSeatsNumber { get; set; }
        [Required]
        [MaxLength(128)]
        public string PlaneType { get; set; }
        public List<Seat> ListOfSeats { get; set; }
    }

}
