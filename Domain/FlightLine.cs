﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain
{
    public class FlightLine
    {
        public int FlightLineId { get; set; }
        public int Airport1Id { get; set; }
        //[ForeignKey("Airport1Id")]
        public Airport Airport1 { get; set; }
        public int Airport2Id { get; set; }
        //[ForeignKey("Airport2Id")]
        public Airport Airport2 { get; set; }
        [DataType(DataType.Date)]

        public DateTime FlightLineStart { get; set; } = new DateTime();
        [DataType(DataType.Date)]
        
        public DateTime FlightLineEnd { get; set; } = new DateTime();

    }
}
