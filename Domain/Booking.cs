﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Booking
    {
        public int BookingId { get; set; }
        public int SeatId { get; set; }
        public Seat Seat { get; set; }
        public int PersonId { get; set; }
        public Person Person { get; set; } = new Person();
        public int FlightId { get; set; }
        public Flight Flight { get; set; }

    }
}
