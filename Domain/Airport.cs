﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Airport
    {

        public int AirportId { get; set; }
        [MaxLength(128)]
        [Required]
        public string AirportName { get; set; }
        [MaxLength(128)]
        [Required]
        public string Location { get; set; }


    }
}
