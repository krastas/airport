﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Seat
    {
        public int SeatId { get; set; }
        public int PlaneId { get; set; }
        public Plane Plane { get; set; }
        public int SeatNumber { get; set; }
        public int PersonInSeatId { get; set; }
        public List<PersonInSeat> PeopleinSeats { get; set; } = new List<PersonInSeat>();
    }
}
