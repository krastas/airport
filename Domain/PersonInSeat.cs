﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class PersonInSeat
    {
        public int PersonInSeatId { get; set; }
        public int PersonId { get; set; }
        public Person Person { get; set; }
        public int SeatId { get; set; }
        public Seat Seat { get; set; }
        public int FlightId { get; set; }
        public Flight Flight { get; set; }

    }
}
